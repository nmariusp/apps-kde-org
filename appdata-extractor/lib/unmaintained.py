# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
import os.path

import yaml


class Unmaintained:
    # projects_w_repo, projects_wo_repo: use the 'Project.repo' field in the frontend
    # projects:
    #   - if a project is not in 'unmaintained' group, consider it as unmaintained and include it (if not already)
    #   - if a project is in 'unmaintained' group, include it
    with open('unmaintained_projects.yaml') as f_p:
        projects_w_repo, projects_wo_repo = yaml.safe_load_all(f_p)
        projects = projects_w_repo + projects_wo_repo
    # apps: set these as unmaintained and include them
    with open('unmaintained_apps.yaml') as f_a:
        apps = yaml.safe_load(f_a)

    # if something is considered unmaintained and local, look for its appdata and desktop files in this directory
    local_storage = 'appdata-unmaintained'
    # app ids to be considered unmaintained and local
    locally_stored_ids = set(map(lambda name: os.path.splitext(name)[0],
                                 filter(lambda name: name.endswith('.appdata'), os.listdir(local_storage))))

    @classmethod
    def with_local_id(cls, name: str) -> str:
        for local_id in cls.locally_stored_ids:
            if local_id.split('.', 2)[2] == name:
                return local_id
        return ''
