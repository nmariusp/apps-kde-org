---
custom: true # dummy just to not have an empty frontmatter
---

{{< container class="text-center text-small" >}}

![](https://apps.kde.org/app-icons/org.kde.kleopatra.svg)
{width="128", height="128"}

# Kleopatra

Kleopatra is an open-source certificate manager and graphical front-end for
cryptographic services, primarily designed to handle OpenPGP (Pretty Good
Privacy) and S/MIME (Secure/Multipurpose Internet Mail Extensions)
certificates. Part of the KDE ecosystem, it provides an accessible interface
for managing encryption keys, signing and verifying data, and encrypting or
decrypting files and emails.

{{< /container >}}

{{< feature-container img="/screenshots/kleopatra/certificates.png" img-class="none" >}}

## Key Management

* **Create, Import, Export Keys**: Users can create new public and private
  encryption keys, import existing ones, and export keys to share with others.
* **Key Servers**: Kleopatra can connect to public key servers, allowing users
  to look up and import public keys from others or upload their own public keys
  to make them available globally.
* **LDAP and WKD**: Aside from key servers, Kleopatra can also connect to LDAP and
  WKD (Web Key Directory) servers and fetch certificates from there.
* **Trust and Expiry Management**: Users can set trust levels for keys and
  manage expiration dates to ensure only trusted keys are in use.

{{< /feature-container >}}

{{< feature-container img="/screenshots/kleopatra/file-encryption.png" reverse="true" img-class="w-75" >}}

## Data Encryption and Decryption

- **Encrypt Files and Texts**: With Kleopatra, users can encrypt files and texts
to protect the content and ensure it is only accessible by recipients with the
correct private key.
- **Decrypt Content**: It supports decryption for files,
text and emails, making it easy for users to access data they received
securely.
- **Digital Signatures**: Kleopatra lets users sign files or notes with
their private key, which helps verify the authenticity of the sender and
integrity of the data.

{{< /feature-container >}}

{{< feature-container img="/screenshots/kleopatra/details.png" img-class="none" >}}

## Support for OpenPGP and S/MIME

* **OpenPGP**: Commonly used in personal and enterprise settings for encrypting and signing emails and files. Kleopatra provides full support for creating, managing, and using OpenPGP certificates.

* **S/MIME**: Often used in enterprise environments for email security. Kleopatra supports S/MIME certificates, enabling it to handle encryption and signing based on the [CMS](https://en.m.wikipedia.org/wiki/Cryptographic_Message_Syntax) standard.

{{< /feature-container >}}

{{< feature-container img="/screenshots/kleopatra/smartcard.png" img-class="none" reverse="true" >}}

## Support for OpenPGP and PIV Smartcards

* **Manage your Smartcard**: Kleopatra lets users change their the PIN and PUK code of the smartcard, change metadata like the cardholder's name, copy existing private keys to the card and even generate directly new keys on the card.
* **Authentification Keys**: Create and use authentification keys from smartcards for SSH logins and more.
* **Compatibility**: Kleopatra is compatible with Yubikey, [Gnuk](https://www.fsif.org/doc-gnuk/) and other cards without the need for extra device drivers.

{{< /feature-container >}}

{{< feature-container img="/screenshots/kleopatra/dolphin-integration.png" img-class="none" >}}

## Integration and Usability

* **Gpg4win Compatibility**: On Windows, Kleopatra is bundled as part of the [Gpg4win suite](https://gpg4win.org), making it the primary tools for managing GnuPG (GPG) encryption on Windows. Kleopatra is also bundled as part of [GnuPG Desktop®️](https://gnupg.com/gnupg-desktop.html) which comes with enterprise support and as part of [GnuPG VS-Desktop®️](https://gnupg.com/gnupg-vs-desktop.html) which is compliant for use with EU and NATO RESTRICTED material and the german VS-NfD.
* **Seamless Integration with KDE Applications**: For Linux users, Kleopatra integrates well within the KDE Plasma, working alongside other KDE apps like [KMail](https://kontact.kde.org/components/kmail/) for secure document handling.
* **File Encryption from File Managers**: It enables direct encryption, signing, and verification of files from file managers, making it very user-friendly for everyday use.

{{< /feature-container >}}

{{< feature-container img="/screenshots/kleopatra/notepad.png" img-class="none" reverse="true" >}}

## User-Friendly Interface

* **Graphical Interface**: Kleopatra provides a GUI to manage cryptographic tasks, which is beneficial for users unfamiliar with command-line tools like GPG.
* **Notepad**: Encrypt, decrypt and verify texts with the Notepad. Copy and paste encrypted text to possibly insecure or unreliable messaging and chat services.
* **Alerts**: Kleopatra notifies users about key status, trust levels, and any potential issues, providing insights for users to manage security settings appropriately.
* **Configuration**: Configure Kleopatra to your needs. kleopatra's configurations can be centrally configured to meet organizational requirements.

{{< /feature-container >}}
