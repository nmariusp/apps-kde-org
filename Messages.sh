#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-3.0-or-later

hugoi18n extract $podir
